{ config, pkgs, ... }:

{
  imports = [
      /etc/nixos/hardware-configuration.nix
      ./xmonad.nix
      ./t420.nix
  ];

  boot.loader.gummiboot.enable = true;
  networking.hostName = "nixos"; # Define your hostname.
  networking.hostId = "11642b24";
  networking.wireless.enable = true;  # Enables wireless.

  # Select internationalisation properties.

  users.defaultUserShell = "/var/run/current-system/sw/bin/zsh";

  nixpkgs.config.allowUnfree = true;
  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget

  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;

  environment.shellInit = ''
    export NIXPKGS_ALLOW_UNFREE=1
    xsetroot -cursor_name left_ptr
    eval $(cat ~/.fehbg)
  '';

  environment.systemPackages = with pkgs; [
    mpd
    tree
    wget
    vim
    emacs
    firefox
    feh
    scrot
    gitAndTools.gitFull
    htop
    zsh
    cmake
    colordiff
    gcc
    gdb
    gnumake
    lynx
    man
    rtorrent
    ruby
    tcpdump
    telnet
    unzip
    vlc
    zip
    rxvt_unicode
    unetbootin
    xorg.xkill
    wpa_supplicant_gui
    xfontsel
    xlibs.xmodmap
    skype
    keepassx2
    dropbox
    leiningen
    erlang
    stack
    stow
    silver-searcher
    mpv
  ];

  fonts = {
    enableCoreFonts = true;
    enableFontDir = true;

    fonts = [
      pkgs.corefonts
      pkgs.inconsolata
      pkgs.source-code-pro
      pkgs.dejavu_fonts
      pkgs.terminus_font
      pkgs.liberation_ttf
      pkgs.xlibs.fontbh100dpi
      pkgs.xlibs.fontbh75dpi
      pkgs.xlibs.fontbhlucidatypewriter100dpi
      pkgs.xlibs.fontbhlucidatypewriter75dpi
      pkgs.xlibs.fontbhttf
      pkgs.xlibs.fontbhtype1
      pkgs.xlibs.fontbitstream100dpi
      pkgs.xlibs.fontbitstream75dpi
      pkgs.xlibs.fontbitstreamtype1
      pkgs.xlibs.fontcronyxcyrillic
      pkgs.xlibs.fontcursormisc
      pkgs.xlibs.fontdaewoomisc
      pkgs.xlibs.fontdecmisc
      pkgs.xlibs.fontibmtype1
      pkgs.xlibs.fontisasmisc
      pkgs.xlibs.fontjismisc
      pkgs.xlibs.fontmicromisc
      pkgs.xlibs.fontmisccyrillic
      pkgs.xlibs.fontmiscethiopic
      pkgs.xlibs.fontmiscmeltho
      pkgs.xlibs.fontmiscmisc
      pkgs.xlibs.fontmuttmisc
      pkgs.xlibs.fontschumachermisc
      pkgs.xlibs.fontscreencyrillic
      pkgs.xlibs.fontsonymisc
      pkgs.xlibs.fontsunmisc
      pkgs.xlibs.fontwinitzkicyrillic
      pkgs.xlibs.fontxfree86type1
    ];
  };
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

   services.postgresql.enable = true;
   services.postgresql.package = pkgs.postgresql;
   services.postgresql.authentication = "local all all ident";

  virtualisation.virtualbox.host.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  services.xserver = {
    layout = "us, ru(winkeys)";
    xkbOptions = "grp:lwin_toggle,ctrl:nocaps";
    xkbVariant = "winkeys";
    synaptics.enable = true;
    synaptics.twoFingerScroll = true;
    synaptics.palmDetect = true;
    synaptics.fingersMap = [ 1 3 2];
  };


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.bubs = {
    home = "/home/bubs";
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "networkmanager" "vboxusers"];
  };

  time.timeZone = "Europe/Minsk";

}
