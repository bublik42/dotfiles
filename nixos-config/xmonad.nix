{ config, pkgs, ... }:

{ 

    services.xserver = {
        enable = true;
        windowManager.xmonad.enable = true;
        windowManager.xmonad.enableContribAndExtras = true;
        windowManager.default = "xmonad";
        desktopManager.xterm.enable = false;
        desktopManager.default = "none";
    };

    environment.systemPackages = with pkgs; [
        dmenu
        dzen2
        conky
        gmrun
        vnstat
        xdotool
        tint2
        hsetroot
        compton
        haskellPackages.xmonad
        haskellPackages.xmobar
        haskellPackages.ghc
    ];
}
    
