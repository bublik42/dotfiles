{ config, pkgs, ... }:

{
    environment.systemPackages = [
        pkgs.i3status
        pkgs.i3lock
        pkgs.dmenu
    ];


    services.xserver = {
        enable = true;
        windowManager.default = "i3";
        windowManager.i3.enable = true;
    };
}
