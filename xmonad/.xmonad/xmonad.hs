import XMonad
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import System.Exit
import Graphics.X11.Xlib
import Graphics.X11.ExtraTypes.XF86
--import IO (Handle, hPutStrLn)
import qualified System.IO
import XMonad.Actions.CycleWS (nextScreen,prevScreen)
import Data.List
-- Prompts
import XMonad.Prompt
import XMonad.Prompt.Shell
-- Actions
import XMonad.Actions.MouseGestures
import XMonad.Actions.SpawnOn
import XMonad.Actions.UpdatePointer
import XMonad.Actions.GridSelect
-- Utils
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.Loggers
import XMonad.Util.EZConfig
import XMonad.Util.Scratchpad
-- Hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.Place
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers
-- Layouts
import XMonad.Layout.Simplest
import XMonad.Layout.MultiColumns
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.DragPane
import XMonad.Layout.LayoutCombinators hiding ((|||))
import XMonad.Layout.DecorationMadness
import XMonad.Layout.TabBarDecoration
import XMonad.Layout.IM
import XMonad.Layout.Grid
import XMonad.Layout.Spiral
import XMonad.Layout.Mosaic
import XMonad.Layout.LayoutHints
import XMonad.Layout.PerWorkspace
import Data.Ratio ((%))
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.Spacing
import XMonad.Layout.Renamed
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.Gaps
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Util.Replace
import Data.Default

myLayout =
    onWorkspace "Steam" (noBorders Simplest) $
    smartBorders $
    smartSpacing 10 $
    -- toggleLayouts (noBorders Full) $
    Tall 1 (3/100) (1/2) ||| Full ||| Grid

myWorkspaces :: [String]
myWorkspaces = map show [1..5] ++ ["Steam"]

myManageHook =
  (isFullscreen --> doFullFloat) <+>
  (className =? "Steam" --> doShift "Steam") <+>
  (className =? "Civ5XP" --> doFloat) <+>
  manageHook def

-- gruvbox colors
darkGrey = "#282828"
lightGrey = "#928374"
violette = "#d3869b"
green = "#98971a"

-- tuffle colors

background = "#111111"
foreground = "#fffff8"
highlight = "#FFF1AA"

myLogHook h = dynamicLogWithPP $ def {
        ppCurrent = dzenColor highlight "" . wrap "[" "]"
      , ppLayout = (\x -> case x of
          "SmartSpacing 10 Tall"                   -> "[T]"
          "SmartSpacing 10 Full"                   -> "[F]"
          "SmartSpacing 10 Grid"                   -> "[+]"
          _                                        -> x)
      , ppSep = " "
      , ppWsSep = " "
      , ppTitle = dzenColor highlight "" . shorten 100
      , ppOutput = System.IO.hPutStrLn h
}

dzenOptions = " -e 'onstart=lower' -h '16' -fn 'Source Code Pro:size=9' -bg " ++ background ++ " -fg " ++ foreground
dzenCommand = "dzen2 -ta l -w 583" ++ dzenOptions
dzen2Command = "~/.cabal/bin/monky | dzen2 -ta r -w 784 -x 582" ++ dzenOptions

main = do
  dzen1 <- spawnPipe dzenCommand
  dzen2 <- spawnPipe dzen2Command

  xmonad $ def {
      terminal    = "urxvt"
    , modMask     = mod1Mask
    , borderWidth = 3
    , workspaces = myWorkspaces
    , layoutHook = avoidStruts $ myLayout
    , focusedBorderColor = darkGrey
    , normalBorderColor = lightGrey
    , manageHook = manageDocks <+> myManageHook
    , logHook = myLogHook dzen1
  }

